《王牌对王牌》  1.2  
《圈子圈套 3》（王强） 1.2  
《无间道2》 1.2  
《死神来了4》 1.3  
《柯南剧场版12 战栗的乐谱》 1.3  
《入殓师》 1.3  
《做最好的自己》 1.3  
《风之谷》 1.4  
《迷雾》 1.5  
《楚门的世界》 1.6  
《无间道3》 1.7  
《不可征服》 1.8  
《英雄 第二季》 1.9  
《飞越疯人院》 1.10  
《关于莉莉周的一切》 1.11  
《天空之城》 1.11  
《柯南剧场版13 漆黑的追踪者》 1.12  
《天使爱美丽》 1.13  
《阿凡达》 1.14  
《搏击俱乐部》 1.14  
《灵异第六感》 1.15  
《越狱 第四季》 2.10  
《粉红豹》 3.6  
《最后一越》 3.6  
《仙剑奇侠传3》（连续剧） 3.7  
《我的野蛮女友》 3.14  
《小姐好白》 3.25  
《大兵小将》 3.26  
《蝴蝶效应》 3.27  
《拆弹部队》 3.28  
《坏中尉》 3.29  
《美丽心灵》4.3  
《金氏漂流记》 4.5  
《记忆碎片》 4.7  
《无耻混蛋》 4.11  
《三枪拍案惊奇》 4.13  
《詹姆斯自传 不只是一场比赛》 4.16  
《迫在眉梢》 4.18  
《越光宝盒》 4.19  
《这个杀手不太冷》 4.21  
《火影忍者剧场版 火之意志继承者》 5.3  
《珍爱》 5.3  
《返老还童》 5.5  
《记忆裂痕》 5.7  
《叶问2》 5.8  
《夺狱困兽》 5.8  
《宿醉》 5.9  
《十二怒汉大审判》 5.11  
《黑客帝国1》 5.13  
《陪你到最终》 5.15  
《百万富翁的初恋》 5.16  
《良医妙药》 5.17  
《守法公民》 5.17  
《黑客帝国2》 5.19  
《忠犬八公的故事》 5.23  
《心灵捕手》 5.24  
《维京英灵殿》 5.26  
《放风筝的人》 5.28  
《人狗奇缘》 5.29  
《加油站被袭事件》 5.30  
《黑客帝国3》 5.30  
《七磅》 6.7  
《虎口脱险》 6.9  
《上帝之城》 6.10  
《周星驰外传》（窦欣平） 6.10  
《人类死刑大观》（马丁·莫内斯蒂埃） 6.17  
《大鱼》 6.13  
《阻击电话亭》 6.14  
《人工智能》 6.14  
《神秘之队》 6.15  
《辛德勒的名单》 6.17  
《僵尸新娘》 6.17  
《绝岭雄风》 6.19  
《美国毒枭》 6.20  
《公众之敌》 6.23  
《人在囧途》 6.24  
《六轮枪强盗团》 6.26  
《神奇遥控器》 6.27  
《怒火救援》 6.27  
《老爷车》 6.28  
《窃听风云》 6.30  
《两只大烟枪》 6.30  
《浪潮》 7.2  
《金鸡》 7.6  
《金鸡2》 7.7  
《猜火车》 7.13  
《雨人》 7.20  
《加油站袭击事件》 8.3  
《史密斯夫妇》 8.4  
《死亡诗社》 8.7  
《我是传奇》 8.11  
《热血高校》 8.15  
《木乃伊3 龙帝之墓》 8.15  
《追随》 8.17  
《死亡实验》 8.19  
《狗镇》 8.22  
《海豚湾》 8.25  
《自由作家日记》 9.8  
《LOST 第六季》 9  
《沙漠之花》 9.12  
《全民公敌》 9.13  
《死亡幻觉》 9.15  
《四兄弟》 9.21  
《英雄 第四季》 9.21  
《终极斗士3 救赎》 9.24  
《人生》 （路遥） 9.24  
《你怎么也想不到》 （路遥） 9.26  
《你怎么也想不到》 （路遥） 9.26  
《全民情敌》 9.27  
《在苦难的日子里》 （路遥） 9.28  
《我和五叔的六次相遇》 （路遥） 9.29  
《黄叶在秋风中飘落》 （路遥） 10.1  
《热血高校2》 10.7  
《惊心动魄的一幕》 （路遥） 10.8  
《摩托日记》 10.12  
《路遥短篇文集》 （路遥） 10.12  
《敢死队》 10.16  
《撞车》 10.24  
《万能钥匙》 10.26  
《四头狮子》 10.27  
《新猛鬼街》 10.30  
《阿呆和阿瓜》 11.6  
《十一罗汉》 11.7  
《十二罗汉》 11.21  
《暗算》（麦家） 11.21  
《11度青春之 老男孩》 11.21  
《末代皇帝》 11.26  
《盗梦空间》 11.28  
《芙蓉镇》（古华） 12.1  
《社交网络》 12.3  
《逍遥法外》 12.3  
《十三罗汉》 12.4  
《精武风云》 12.8  
《美丽心灵的永恒阳光》 12.11  
《攻壳机动队》 12.13  
《惊声尖叫1》 12.14  
《窗边的小豆豆》 （黑柳彻子） 12.14  
《角斗士》 12.15  
《生化危机4》 12.16  
《波斯王之 时之刃》 12.18  
《帝国太阳》 12.19  
《大叔》 12.20  
《憨豆先生的假期》 12.21  
《让子弹飞》 12.22  
《杀死比尔》 12.25  
《美国派6 兄弟会》 12.25  
《神奇四侠2》 12.26  
《英雄传说 空之轨迹FC》 12.27  
《天国王朝》 12.28  
《低俗小说》 12.29  
